# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/fraromesc/ws_usv/src/usv_simulator/usv_gazebo_plugins/include".split(';') if "/home/fraromesc/ws_usv/src/usv_simulator/usv_gazebo_plugins/include" != "" else []
PROJECT_CATKIN_DEPENDS = "message_runtime;gazebo_dev;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "usv_gazebo_plugins"
PROJECT_SPACE_DIR = "/home/fraromesc/ws_usv/devel"
PROJECT_VERSION = "0.3.2"
