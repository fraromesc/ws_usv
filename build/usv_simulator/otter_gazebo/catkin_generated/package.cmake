set(_CATKIN_CURRENT_PACKAGE "otter_gazebo")
set(otter_gazebo_VERSION "0.0.0")
set(otter_gazebo_MAINTAINER "Jan Henrik Lenes <jhlenes@outlook.com>")
set(otter_gazebo_PACKAGE_FORMAT "2")
set(otter_gazebo_BUILD_DEPENDS "gazebo_plugins" "hector_gazebo_plugins" "robot_localization" "usv_gazebo_plugins" "velodyne_gazebo_plugins" "velodyne_description" "otter_description" "xacro" "message_filters" "roscpp" "teleop_twist_keyboard")
set(otter_gazebo_BUILD_EXPORT_DEPENDS "gazebo_plugins" "hector_gazebo_plugins" "robot_localization" "usv_gazebo_plugins" "velodyne_gazebo_plugins" "velodyne_description" "otter_description" "xacro" "message_filters" "roscpp" "teleop_twist_keyboard")
set(otter_gazebo_BUILDTOOL_DEPENDS "catkin")
set(otter_gazebo_BUILDTOOL_EXPORT_DEPENDS )
set(otter_gazebo_EXEC_DEPENDS "gazebo_plugins" "hector_gazebo_plugins" "robot_localization" "usv_gazebo_plugins" "velodyne_gazebo_plugins" "velodyne_description" "otter_description" "xacro" "message_filters" "roscpp" "teleop_twist_keyboard")
set(otter_gazebo_RUN_DEPENDS "gazebo_plugins" "hector_gazebo_plugins" "robot_localization" "usv_gazebo_plugins" "velodyne_gazebo_plugins" "velodyne_description" "otter_description" "xacro" "message_filters" "roscpp" "teleop_twist_keyboard")
set(otter_gazebo_TEST_DEPENDS )
set(otter_gazebo_DOC_DEPENDS )
set(otter_gazebo_URL_WEBSITE "")
set(otter_gazebo_URL_BUGTRACKER "")
set(otter_gazebo_URL_REPOSITORY "")
set(otter_gazebo_DEPRECATED "")