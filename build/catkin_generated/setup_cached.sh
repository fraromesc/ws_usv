#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/fraromesc/ws_usv/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/fraromesc/ws_usv/build'
export ROSLISP_PACKAGE_DIRECTORIES="/home/fraromesc/ws_usv/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/fraromesc/ws_usv/src:$ROS_PACKAGE_PATH"