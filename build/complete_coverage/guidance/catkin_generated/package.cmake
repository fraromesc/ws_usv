set(_CATKIN_CURRENT_PACKAGE "guidance")
set(guidance_VERSION "0.0.0")
set(guidance_MAINTAINER "henrik <henrik@todo.todo>")
set(guidance_PACKAGE_FORMAT "2")
set(guidance_BUILD_DEPENDS "roscpp" "nav_msgs" "geometry_msgs" "tf2" "tf2_ros" "usv_msgs")
set(guidance_BUILD_EXPORT_DEPENDS "roscpp" "nav_msgs" "geometry_msgs" "tf2" "tf2_ros" "usv_msgs")
set(guidance_BUILDTOOL_DEPENDS "catkin")
set(guidance_BUILDTOOL_EXPORT_DEPENDS )
set(guidance_EXEC_DEPENDS "roscpp" "nav_msgs" "geometry_msgs" "tf2" "tf2_ros" "usv_msgs")
set(guidance_RUN_DEPENDS "roscpp" "nav_msgs" "geometry_msgs" "tf2" "tf2_ros" "usv_msgs")
set(guidance_TEST_DEPENDS )
set(guidance_DOC_DEPENDS )
set(guidance_URL_WEBSITE "")
set(guidance_URL_BUGTRACKER "")
set(guidance_URL_REPOSITORY "")
set(guidance_DEPRECATED "")