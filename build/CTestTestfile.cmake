# CMake generated Testfile for 
# Source directory: /home/fraromesc/ws_usv/src
# Build directory: /home/fraromesc/ws_usv/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("complete_coverage/otter_slam")
subdirs("complete_coverage/usv_msgs")
subdirs("complete_coverage/mr_obs_connector")
subdirs("complete_coverage/coverage_boustrophedon")
subdirs("complete_coverage/coverage_binn")
subdirs("complete_coverage/guidance")
subdirs("usv_simulator/otter_control")
subdirs("usv_simulator/usv_gazebo_plugins")
subdirs("usv_simulator/otter_description")
subdirs("usv_simulator/otter_gazebo")
subdirs("usv_simulator/usv_worlds")
subdirs("complete_coverage/map_inflating")
subdirs("complete_coverage/sensors")
