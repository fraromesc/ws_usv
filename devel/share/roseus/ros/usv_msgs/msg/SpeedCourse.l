;; Auto-generated. Do not edit!


(when (boundp 'usv_msgs::SpeedCourse)
  (if (not (find-package "USV_MSGS"))
    (make-package "USV_MSGS"))
  (shadow 'SpeedCourse (find-package "USV_MSGS")))
(unless (find-package "USV_MSGS::SPEEDCOURSE")
  (make-package "USV_MSGS::SPEEDCOURSE"))

(in-package "ROS")
;;//! \htmlinclude SpeedCourse.msg.html


(defclass usv_msgs::SpeedCourse
  :super ros::object
  :slots (_speed _course ))

(defmethod usv_msgs::SpeedCourse
  (:init
   (&key
    ((:speed __speed) 0.0)
    ((:course __course) 0.0)
    )
   (send-super :init)
   (setq _speed (float __speed))
   (setq _course (float __course))
   self)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:course
   (&optional __course)
   (if __course (setq _course __course)) _course)
  (:serialization-length
   ()
   (+
    ;; float64 _speed
    8
    ;; float64 _course
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _course
       (sys::poke _course (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _speed
     (setq _speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _course
     (setq _course (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get usv_msgs::SpeedCourse :md5sum-) "5ae1367da119f48c1bc62eecb750d210")
(setf (get usv_msgs::SpeedCourse :datatype-) "usv_msgs/SpeedCourse")
(setf (get usv_msgs::SpeedCourse :definition-)
      "float64 speed   # Desired surge speed  [m/s]
float64 course  # Desired course angle [rad]

")



(provide :usv_msgs/SpeedCourse "5ae1367da119f48c1bc62eecb750d210")


