;; Auto-generated. Do not edit!


(when (boundp 'coverage_boustrophedon::DubinInput)
  (if (not (find-package "COVERAGE_BOUSTROPHEDON"))
    (make-package "COVERAGE_BOUSTROPHEDON"))
  (shadow 'DubinInput (find-package "COVERAGE_BOUSTROPHEDON")))
(unless (find-package "COVERAGE_BOUSTROPHEDON::DUBININPUT")
  (make-package "COVERAGE_BOUSTROPHEDON::DUBININPUT"))

(in-package "ROS")
;;//! \htmlinclude DubinInput.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass coverage_boustrophedon::DubinInput
  :super ros::object
  :slots (_header _start _end ))

(defmethod coverage_boustrophedon::DubinInput
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:start __start) (instance geometry_msgs::PoseStamped :init))
    ((:end __end) (instance geometry_msgs::PoseStamped :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _start __start)
   (setq _end __end)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:start
   (&rest __start)
   (if (keywordp (car __start))
       (send* _start __start)
     (progn
       (if __start (setq _start (car __start)))
       _start)))
  (:end
   (&rest __end)
   (if (keywordp (car __end))
       (send* _end __end)
     (progn
       (if __end (setq _end (car __end)))
       _end)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/PoseStamped _start
    (send _start :serialization-length)
    ;; geometry_msgs/PoseStamped _end
    (send _end :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/PoseStamped _start
       (send _start :serialize s)
     ;; geometry_msgs/PoseStamped _end
       (send _end :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/PoseStamped _start
     (send _start :deserialize buf ptr-) (incf ptr- (send _start :serialization-length))
   ;; geometry_msgs/PoseStamped _end
     (send _end :deserialize buf ptr-) (incf ptr- (send _end :serialization-length))
   ;;
   self)
  )

(setf (get coverage_boustrophedon::DubinInput :md5sum-) "fbb72dfce16530646ad15d19f75ccf8b")
(setf (get coverage_boustrophedon::DubinInput :datatype-) "coverage_boustrophedon/DubinInput")
(setf (get coverage_boustrophedon::DubinInput :definition-)
      "# start must have both position and orientation.
# the orientation of end is not used.
Header header
geometry_msgs/PoseStamped start
geometry_msgs/PoseStamped end
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/PoseStamped
# A Pose with reference coordinate frame and timestamp
Header header
Pose pose

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :coverage_boustrophedon/DubinInput "fbb72dfce16530646ad15d19f75ccf8b")


