
(cl:in-package :asdf)

(defsystem "usv_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "SpeedCourse" :depends-on ("_package_SpeedCourse"))
    (:file "_package_SpeedCourse" :depends-on ("_package"))
  ))