; Auto-generated. Do not edit!


(cl:in-package usv_msgs-msg)


;//! \htmlinclude SpeedCourse.msg.html

(cl:defclass <SpeedCourse> (roslisp-msg-protocol:ros-message)
  ((speed
    :reader speed
    :initarg :speed
    :type cl:float
    :initform 0.0)
   (course
    :reader course
    :initarg :course
    :type cl:float
    :initform 0.0))
)

(cl:defclass SpeedCourse (<SpeedCourse>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SpeedCourse>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SpeedCourse)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name usv_msgs-msg:<SpeedCourse> is deprecated: use usv_msgs-msg:SpeedCourse instead.")))

(cl:ensure-generic-function 'speed-val :lambda-list '(m))
(cl:defmethod speed-val ((m <SpeedCourse>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader usv_msgs-msg:speed-val is deprecated.  Use usv_msgs-msg:speed instead.")
  (speed m))

(cl:ensure-generic-function 'course-val :lambda-list '(m))
(cl:defmethod course-val ((m <SpeedCourse>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader usv_msgs-msg:course-val is deprecated.  Use usv_msgs-msg:course instead.")
  (course m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SpeedCourse>) ostream)
  "Serializes a message object of type '<SpeedCourse>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'course))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SpeedCourse>) istream)
  "Deserializes a message object of type '<SpeedCourse>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'speed) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'course) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SpeedCourse>)))
  "Returns string type for a message object of type '<SpeedCourse>"
  "usv_msgs/SpeedCourse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SpeedCourse)))
  "Returns string type for a message object of type 'SpeedCourse"
  "usv_msgs/SpeedCourse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SpeedCourse>)))
  "Returns md5sum for a message object of type '<SpeedCourse>"
  "5ae1367da119f48c1bc62eecb750d210")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SpeedCourse)))
  "Returns md5sum for a message object of type 'SpeedCourse"
  "5ae1367da119f48c1bc62eecb750d210")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SpeedCourse>)))
  "Returns full string definition for message of type '<SpeedCourse>"
  (cl:format cl:nil "float64 speed   # Desired surge speed  [m/s]~%float64 course  # Desired course angle [rad]~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SpeedCourse)))
  "Returns full string definition for message of type 'SpeedCourse"
  (cl:format cl:nil "float64 speed   # Desired surge speed  [m/s]~%float64 course  # Desired course angle [rad]~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SpeedCourse>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SpeedCourse>))
  "Converts a ROS message object to a list"
  (cl:list 'SpeedCourse
    (cl:cons ':speed (speed msg))
    (cl:cons ':course (course msg))
))
