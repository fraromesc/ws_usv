; Auto-generated. Do not edit!


(cl:in-package coverage_boustrophedon-msg)


;//! \htmlinclude DubinInput.msg.html

(cl:defclass <DubinInput> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (start
    :reader start
    :initarg :start
    :type geometry_msgs-msg:PoseStamped
    :initform (cl:make-instance 'geometry_msgs-msg:PoseStamped))
   (end
    :reader end
    :initarg :end
    :type geometry_msgs-msg:PoseStamped
    :initform (cl:make-instance 'geometry_msgs-msg:PoseStamped)))
)

(cl:defclass DubinInput (<DubinInput>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DubinInput>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DubinInput)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name coverage_boustrophedon-msg:<DubinInput> is deprecated: use coverage_boustrophedon-msg:DubinInput instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <DubinInput>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader coverage_boustrophedon-msg:header-val is deprecated.  Use coverage_boustrophedon-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'start-val :lambda-list '(m))
(cl:defmethod start-val ((m <DubinInput>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader coverage_boustrophedon-msg:start-val is deprecated.  Use coverage_boustrophedon-msg:start instead.")
  (start m))

(cl:ensure-generic-function 'end-val :lambda-list '(m))
(cl:defmethod end-val ((m <DubinInput>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader coverage_boustrophedon-msg:end-val is deprecated.  Use coverage_boustrophedon-msg:end instead.")
  (end m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DubinInput>) ostream)
  "Serializes a message object of type '<DubinInput>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'start) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'end) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DubinInput>) istream)
  "Deserializes a message object of type '<DubinInput>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'start) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'end) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DubinInput>)))
  "Returns string type for a message object of type '<DubinInput>"
  "coverage_boustrophedon/DubinInput")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DubinInput)))
  "Returns string type for a message object of type 'DubinInput"
  "coverage_boustrophedon/DubinInput")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DubinInput>)))
  "Returns md5sum for a message object of type '<DubinInput>"
  "fbb72dfce16530646ad15d19f75ccf8b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DubinInput)))
  "Returns md5sum for a message object of type 'DubinInput"
  "fbb72dfce16530646ad15d19f75ccf8b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DubinInput>)))
  "Returns full string definition for message of type '<DubinInput>"
  (cl:format cl:nil "# start must have both position and orientation.~%# the orientation of end is not used.~%Header header~%geometry_msgs/PoseStamped start~%geometry_msgs/PoseStamped end~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DubinInput)))
  "Returns full string definition for message of type 'DubinInput"
  (cl:format cl:nil "# start must have both position and orientation.~%# the orientation of end is not used.~%Header header~%geometry_msgs/PoseStamped start~%geometry_msgs/PoseStamped end~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/PoseStamped~%# A Pose with reference coordinate frame and timestamp~%Header header~%Pose pose~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DubinInput>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'start))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'end))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DubinInput>))
  "Converts a ROS message object to a list"
  (cl:list 'DubinInput
    (cl:cons ':header (header msg))
    (cl:cons ':start (start msg))
    (cl:cons ':end (end msg))
))
