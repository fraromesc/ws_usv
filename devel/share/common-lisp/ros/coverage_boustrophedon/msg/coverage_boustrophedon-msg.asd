
(cl:in-package :asdf)

(defsystem "coverage_boustrophedon-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "DubinInput" :depends-on ("_package_DubinInput"))
    (:file "_package_DubinInput" :depends-on ("_package"))
  ))